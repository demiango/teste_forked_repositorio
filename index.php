<?php

require_once 'vendor/autoload.php';


use Firebase\JWT\JWT;
use Firebase\JWT\Key;

include_once "con/conexao.php";
//print_r($_POST);


// logout
if (isset($_GET['acao']) && $_GET['acao'] == 'logout') {
	session_destroy($_SESSION['usuario']);
	header("Location: login.php");
}

// login
$email = $_POST['email'];
//$senha = md5($_POST['senha']);
$senha = $_POST['senha'];
$search = $search;
$email = $email;
$sql = "SELECT * FROM usuarios WHERE email=:email AND senha=:senha";
$stmt = $PDO->prepare($sql);
$stmt->bindParam(':email', $email);
$stmt->bindParam(':senha', $senha);
$result = $stmt->execute();
$user = $stmt->fetch(\PDO::FETCH_ASSOC);
if (sizeof($user) > 1) {
	session_start();
	$_SESSION['usuario'] = $user;
	//echo 'logado';
} else {
	session_destroy($_SESSION['usuario']);
	header("Location: login.php");
}

//print_r($_SESSION['usuario']['id']);

// gera token jwt a partir do id do usuário

$key = $_SESSION['usuario']['id'];
$payload = array(
    "iss" => "http://mobisys.org",
    "aud" => "http://mobisys.com.br",
    "iat" => 1356999524,
    "nbf" => 1357000000
);

$jwt = JWT::encode($payload, $key, 'HS256');

//echo $jwt;

// atualiza usuario com token jwt novo


$sql = "UPDATE usuarios set token = :token WHERE id = :id";
$stmt = $PDO->prepare( $sql );
$stmt->bindParam( ':token', $jwt );
$stmt->bindParam( ':id', $_SESSION['usuario']['id'] );
 
$result = $stmt->execute();
 
if ( ! $result )
{
    var_dump( $stmt->errorInfo() );
    exit;
}
 
//echo $stmt->rowCount() . "linhas alteradas";



// busca todos os usuarios e mostra usuario atual em vermelho
$sql2 = "SELECT * FROM usuarios";
$stmt2 = $PDO->prepare($sql2);
$result2 = $stmt2->execute();
$_SESSION['tds_usuarios'] = $stmt2->fetchAll(\PDO::FETCH_ASSOC);


//INSERT INTO `usuarios` (`id`, `email`, `senha`, `data`) VALUES (NULL, 'demianescobar@gmail.com', 'abab1212', '2022-01-21 15:37:58.000000');

?>

<!doctype html>
<html lang="en" dir="ltr">

			<?php include_once "header.php"; ?> 

			<div class="wrapper">
				
				<!-- Sidebar Holder -->
				<?php include_once "menu.php"; ?> 

				
				<!-- Home -->
				<?php include_once "home.php"; ?>

				
			</div>
		</div>

		<!--footer-->
		<?php include_once "footer.php"; ?>
		<!-- End Footer-->
	</div>

	<!-- Back to top -->
	<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>


	<!-- Dashboard Core -->
	<script src="assets/js/vendors/jquery-3.2.1.min.js"></script>
	<script src="assets/plugins/bootstrap-4.1.3/popper.min.js"></script>
	<script src="assets/plugins/bootstrap-4.1.3/js/bootstrap.min.js"></script>
	<script src="assets/js/vendors/jquery.sparkline.min.js"></script>
	<script src="assets/js/vendors/selectize.min.js"></script>
	<script src="assets/js/vendors/jquery.tablesorter.min.js"></script>
	<script src="assets/js/vendors/circle-progress.min.js"></script>
	<script src="assets/plugins/rating/jquery.rating-stars.js"></script>

	<!-- Fullside-menu Js-->
	<script src="assets/plugins/fullside-menu/jquery.slimscroll.min.js"></script>
	<script src="assets/plugins/fullside-menu/waves.min.js"></script>

	<!-- Chart.js -->
	<script src="assets/plugins/Chart.js/Chart.js"></script>

	<!--Counters -->
	<script src="assets/plugins/counters/counterup.min.js"></script>
	<script src="assets/plugins/counters/waypoints.min.js"></script>

	<!--Morris.js Charts Plugin -->
	<script src="assets/plugins/morris/raphael-min.js"></script>
	<script src="assets/plugins/morris/morris.js"></script>

	<!-- Custom scroll bar Js-->
	<script src="assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js"></script>

	<!-- jQuery Sparklines -->
	<script src="assets/plugins/jquery-sparkline/jquery.sparkline.min.js"></script>

	<!-- JQVMap -->
	<script src="assets/plugins/jqvmap/jquery.vmap.js"></script>
	<script src="assets/plugins/jqvmap/maps/jquery.vmap.world.js"></script>
	<script src="assets/plugins/jqvmap/jquery.vmap.sampledata.js"></script>

	<script src="assets/plugins/echarts/echarts.js"></script>
	<script src="assets/js/index1.js"></script>


	<!-- Custom Js-->
	<script src="assets/js/custom.js"></script>
	<script src="assets/js/jqvmap.js"></script>
	<script src="assets/js/chartbundle.js"></script>

	<!--Counters js-->
	<script>
		$('.count').countUp();
	</script>

</body>

</html>