<?php

include_once "con/conexao.php";
//include_once "con/conexao.php";

if (isset($_POST['btn_enviar'])) {

  try {

    $stmt = $PDO->prepare('INSERT INTO usuarios (nome, email, senha) VALUES(:nome, :email, :senha)');

    $stmt->execute(array(
      ':nome' => $_POST["nome"],
      ':email' => $_POST["email"],
      ':senha' => $_POST["senha"],
    ));

    $form_inserted = true;
    //echo $stmt->rowCount();
	// envia para login
	header("Location: index.php");

  } catch (PDOException $e) {
    echo 'Error: ' . $e->getMessage();
  }
}


?>





<!doctype html>
<html lang="en" dir="ltr">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="msapplication-TileColor" content="#4d83ff">
	<meta name="theme-color" content="#4d83ff">
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />

	<!-- Title -->
	<title>Mubisys</title>
	<link rel="stylesheet" href="assets/fonts/fonts/font-awesome.min.css">

	<!-- Font Family-->
	<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700&amp;subset=latin-ext,vietnamese" rel="stylesheet">

	<!-- Bootstrap Css -->
	<link href="assets/plugins/bootstrap-4.1.3/css/bootstrap.min.css" rel="stylesheet" />

	<!-- Sidemenu Css -->
	<link href="assets/plugins/fullside-menu/css/style.css" rel="stylesheet" />
	<link href="assets/plugins/fullside-menu/waves.min.css" rel="stylesheet" />

	<!-- Dashboard css -->
	<link href="assets/css/dashboard.css" rel="stylesheet" />

	<!-- c3.js Charts Plugin -->
	<link href="assets/plugins/charts-c3/c3-chart.css" rel="stylesheet" />

	<!---Font icons-->
	<link href="assets/plugins/iconfonts/plugin.css" rel="stylesheet" />

</head>

<body class="">

	<div id="loading"></div>

	<div class="overlay"></div>
	<div class="background login-img"></div>
	<div class="masthead">
		<div class="masthead-bg"></div>
		<div class="container h-100">
			<div class="row h-100">
				<div class="col-12 my-auto">
					<div class="masthead-content text-dark py-5 py-md-0">
						<form method="post" action="registrar.php">
							<div class="text-center mb-3">
								<!-- <img src="assets/images/brand/logo1.png" class="" alt=""> -->
							</div>
							<div class="card">
								<div class="card-body">
									<div class="card-title text-center text-dark">Mubisys Registrar</div>


									<div class="form-group">
										<label class="form-label text-dark">Nome</label>
										<input type="text" name="nome" class="form-control" placeholder="Nome" required="">

									</div>

									<div class="form-group">
										<label class="form-label text-dark">E-mail</label>
										<input type="text" name="email" class="form-control" placeholder="E-mail" required="">

									</div>
									<div class="form-group">
										<label class="form-label text-dark">Senha</label>
										<input type="password" name="senha" class="form-control" id="exampleInputPassword1" placeholder="Senha" required="">
									</div>
									<div class="form-group">
										<label class="custom-control custom-checkbox">
											<!-- <a href="forgot-password.html" class="float-right small text-dark">I forgot password</a> -->
											<input type="checkbox" class="custom-control-input" />
											<!-- <span class="custom-control-label text-dark">Lembrar-me</span> -->
										</label>
									</div>
									<div class="form-footer mt-2">
										<!-- <a href="index.html" class="btn btn-success btn-block">Entrar</a> -->
										<button type="submit" name="btn_enviar" class="btn btn-primary btn-lg btn-block">Cadastrar</button>
										

									</div>
									<!-- <div class="text-center  mt-3 text-dark">
											Don't have account yet? <a href="register.html">SignUp</a>
										</div> -->


								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>



	<!-- Dashboard js -->
	<script src="assets/js/vendors/jquery-3.2.1.min.js"></script>
	<script src="assets/plugins/bootstrap-4.1.3/popper.min.js"></script>
	<script src="assets/plugins/bootstrap-4.1.3/js/bootstrap.min.js"></script>
	<script src="assets/js/vendors/jquery.sparkline.min.js"></script>
	<script src="assets/js/vendors/selectize.min.js"></script>
	<script src="assets/js/vendors/jquery.tablesorter.min.js"></script>
	<script src="assets/js/vendors/circle-progress.min.js"></script>
	<script src="assets/plugins/rating/jquery.rating-stars.js"></script>

	<!-- Custom scroll bar Js-->
	<script src="assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js"></script>

	<!-- Fullside-menu Js-->
	<script src="assets/plugins/fullside-menu/jquery.slimscroll.min.js"></script>
	<script src="assets/plugins/fullside-menu/waves.min.js"></script>

	<!-- Custom Js-->
	<script src="assets/js/custom.js"></script>
</body>

</html>